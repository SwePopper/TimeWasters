using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class TitleScreen : Ui.Root {
	public TitleScreen(Ui.Manager manager) : base(manager) {}

	public override void Initialize() {
		Ui.Image image = new Ui.Image(manager, this);
		image.texture = new Texture2D(manager.graphicsDevice, 1, 1);
		image.texture.SetData(new Color[] { Color.Black });
		image.SetHightWithAspect(manager.graphicsDevice.Viewport.Width, image.texture);
		image.drawDepth = 0;

		SpriteFont font = contentManager.Load<SpriteFont>("Fonts/Hud");

		Ui.Text text = new Ui.Text(manager, this);
		text.text = "Time Wasters";
		text.font = font;
		text.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - GameMath.FloorToInt(font.MeasureString(text.text).X) / 2;
		text.bounds.Y = manager.graphicsDevice.Viewport.Height / 2 - GameMath.FloorToInt(font.MeasureString(text.text).Y) / 2;
		text.drawDepth = image.drawDepth + 1;

		Ui.Button button = new Ui.Button(manager, this);
		button.bounds.Width = manager.graphicsDevice.Viewport.Width;
		button.bounds.Height = GameMath.FloorToInt(font.MeasureString(text.text).Y) + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = manager.graphicsDevice.Viewport.Height - manager.graphicsDevice.Viewport.Height / 5 - button.bounds.Height / 2;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { Color.Green });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { Color.Maroon });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { Color.Red });
		button.onClickEvent += ToMainMenu;
		button.drawDepth = text.drawDepth + 1;

		text = new Ui.Text(manager, button);
		text.text = "Start";
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

		base.Initialize();
	}

	void ToMainMenu() {
		manager.SetCurrentRoot(new MainMenu(manager));
	}
}

}