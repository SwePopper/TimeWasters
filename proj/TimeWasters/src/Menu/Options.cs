using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class Options : Ui.Root {
	public Options(Ui.Manager manager) : base(manager) {}

	public override void Initialize() {
		Ui.Image image = new Ui.Image(manager, this);
		image.texture = new Texture2D(manager.graphicsDevice, 1, 1);
		image.texture.SetData(new Color[] { Color.Black });
		image.SetHightWithAspect(manager.graphicsDevice.Viewport.Width, image.texture);
		image.drawDepth = 0;

		base.Initialize();
	}

	public override void OnBack() {
		manager.SetCurrentRoot(new MainMenu(manager));
		base.OnBack();
	}
}

}