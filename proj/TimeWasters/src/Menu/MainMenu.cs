using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class MainMenu : Ui.Root {
	Color buttonNormal = Color.DarkGray;
	Color buttonHighlight = Color.LightGray;
	Color buttonPress = Color.Gray;

	public MainMenu(Ui.Manager manager) : base(manager) {}

	public override void Initialize() {
		Ui.Image image = new Ui.Image(manager, this);
		image.texture = new Texture2D(manager.graphicsDevice, 1, 1);
		image.texture.SetData(new Color[] { Color.Black });
		image.SetHightWithAspect(manager.graphicsDevice.Viewport.Width, image.texture);
		image.drawDepth = 0;

		SpriteFont font = contentManager.Load<SpriteFont>("Fonts/Hud");

		int fontTextHeight = GameMath.FloorToInt(font.MeasureString("T").Y);
		int buttonY = manager.graphicsDevice.Viewport.Height / 2;

#region Games Button

		Ui.Button button = new Ui.Button(manager, this);
		button.bounds.Width = manager.graphicsDevice.Viewport.Width;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = buttonY;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { buttonHighlight });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { buttonPress });
		button.onClickEvent += OnGames;

		Ui.Text text = new Ui.Text(manager, button);
		text.text = "Games";
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

#endregion

		buttonY += fontTextHeight + 30;

#region Options

		button = new Ui.Button(manager, this);
		button.bounds.Width = manager.graphicsDevice.Viewport.Width;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = buttonY;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { buttonHighlight });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { buttonPress });
		button.onClickEvent += OnOptions;

		text = new Ui.Text(manager, button);
		text.text = "Options";
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

#endregion

		buttonY += fontTextHeight + 50;

#region Quit

		button = new Ui.Button(manager, this);
		button.bounds.Width = manager.graphicsDevice.Viewport.Width;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = buttonY;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { buttonHighlight });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { buttonPress });
		button.onClickEvent += OnQuit;

		text = new Ui.Text(manager, button);
		text.text = "Quit";
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

#endregion

		base.Initialize();
	}

	void OnGames() {
		manager.SetCurrentRoot(new Games(manager));
	}

	void OnOptions() {
		manager.SetCurrentRoot(new Options(manager));
	}

	void OnQuit() {
		Game1.Instance.Exit();
	}

	public override void OnBack() {
		manager.SetCurrentRoot(new TitleScreen(manager));
		base.OnBack();
	}
}

}