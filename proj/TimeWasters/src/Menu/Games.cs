using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class Games : Ui.Root {
	Color buttonNormal = Color.DarkGray;
	Color buttonHighlight = Color.LightGray;
	Color buttonPress = Color.Gray;

	Ui.Text gameTitleText = null;
	Ui.Text gameInfoText = null;

	class GameInfo {
		public delegate GameBase CreateGameFunction();
		public string title = "";
		public string info = "";
		public CreateGameFunction onCreateGame = null;
	}
	GameInfo[] gameInfos = null;
	int currentGameInfo = 0;

	public Games(Ui.Manager manager) : base(manager) {}

	public override void Initialize() {
#region Game Info

		gameInfos = new GameInfo[] {
			new GameInfo() { title = "Corridor", info = "Dont crash", onCreateGame = () => new Corridor() },
			new GameInfo() { title = "Juggler", info = "Juggle some ballz", onCreateGame = () => new Juggler() },
			new GameInfo() { title = "RougeLike", info = "RRRRR", onCreateGame = () => new RogueLike() },
			new GameInfo() { title = "Platformer", info = "PPPP", onCreateGame = () => new Platformer() }
		};

#endregion

		Ui.Image image = new Ui.Image(manager, this);
		image.texture = new Texture2D(manager.graphicsDevice, 1, 1);
		image.texture.SetData(new Color[] { Color.Black });
		image.SetHightWithAspect(manager.graphicsDevice.Viewport.Width, image.texture);
		image.drawDepth = 0;

		SpriteFont font = contentManager.Load<SpriteFont>("Fonts/Hud");
		int fontTextHeight = GameMath.FloorToInt(font.MeasureString("T").Y);

#region Left

		string buttonText = "Left";

		Ui.Button button = new Ui.Button(manager, this);
		button.bounds.Width = GameMath.FloorToInt(font.MeasureString(buttonText).X) + 20;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = button.bounds.Width / 2;
		button.bounds.Y = manager.graphicsDevice.Viewport.Height / 2 - button.bounds.Height / 2;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.onClickEvent += OnLeft;

		Ui.Text text = new Ui.Text(manager, button);
		text.text = buttonText;
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

#endregion

#region Center

		gameTitleText = new Ui.Text(manager, this);
		gameTitleText.text = "";
		gameTitleText.font = font;
		gameTitleText.bounds.Y = manager.graphicsDevice.Viewport.Height / 10;

		gameInfoText = new Ui.Text(manager, this);
		gameInfoText.text = "";
		gameInfoText.font = font;
		gameInfoText.bounds.Y = gameTitleText.bounds.Y + GameMath.FloorToInt(gameTitleText.font.MeasureString("T").Y) + 30;

#endregion

#region Right

		buttonText = "Right";

		button = new Ui.Button(manager, this);
		button.bounds.Width = GameMath.FloorToInt(font.MeasureString(buttonText).X) + 20;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width - button.bounds.Width - button.bounds.Width / 2;
		button.bounds.Y = manager.graphicsDevice.Viewport.Height / 2 - button.bounds.Height / 2;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.onClickEvent += OnRight;

		text = new Ui.Text(manager, button);
		text.text = buttonText;
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

#endregion

#region Start Button

		buttonText = "Start Game";

		button = new Ui.Button(manager, this);
		button.bounds.Width = GameMath.FloorToInt(font.MeasureString(buttonText).X) + 20;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = manager.graphicsDevice.Viewport.Height - button.bounds.Height * 3;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { buttonHighlight });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { buttonPress });
		button.onClickEvent += OnStartGame;

		text = new Ui.Text(manager, button);
		text.text = buttonText;
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

		manager.Select(button);

#endregion

		Select(0);

		manager.input.onMovedLeft += OnLeft;
		manager.input.onMovedRight += OnRight;

		base.Initialize();
	}

	void Select(int gameInfo) {
		currentGameInfo = GameMath.Clamp(gameInfo, 0, gameInfos.Length - 1);
		gameTitleText.text = gameInfos[currentGameInfo].title;
		gameTitleText.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - GameMath.FloorToInt(gameTitleText.font.MeasureString(gameTitleText.text).X) / 2;

		gameInfoText.text = gameInfos[currentGameInfo].info;
		gameInfoText.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - GameMath.FloorToInt(gameInfoText.font.MeasureString(gameInfoText.text).X) / 2;
	}

	public override void OnBack() {
		manager.SetCurrentRoot(new MainMenu(manager));
		base.OnBack();
	}

	void OnLeft() {
		Select(currentGameInfo - 1);
	}

	void OnRight() {
		Select(currentGameInfo + 1);
	}

	void OnStartGame() {
		Game1.Instance.StartGame(gameInfos[currentGameInfo].onCreateGame.Invoke());
		manager.SetCurrentRoot(null);
	}
}

}