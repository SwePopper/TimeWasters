using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class Pause : Ui.Root {
	Color buttonNormal = Color.DarkGray;
	Color buttonHighlight = Color.LightGray;
	Color buttonPress = Color.Gray;

	public Pause(Ui.Manager manager) : base(manager) {}

	public override void Initialize() {
		Ui.Image image = new Ui.Image(manager, this);
		image.texture = new Texture2D(manager.graphicsDevice, 1, 1);
		image.texture.SetData(new Color[] { Color.Black });
		image.SetHightWithAspect(manager.graphicsDevice.Viewport.Width, image.texture);
		image.drawDepth = 0;

		SpriteFont font = contentManager.Load<SpriteFont>("Fonts/Hud");
		int fontTextHeight = GameMath.FloorToInt(font.MeasureString("T").Y);

		Ui.Text text = new Ui.Text(manager, this);
		text.text = "Paused";
		text.font = font;
		text.bounds.Y = manager.graphicsDevice.Viewport.Height / 3;
		text.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X) / 2;

		Ui.Button button = new Ui.Button(manager, this);
		button.bounds.Width = manager.graphicsDevice.Viewport.Width;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = manager.graphicsDevice.Viewport.Height / 2;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { buttonHighlight });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { buttonPress });
		button.onClickEvent += OnResume;

		text = new Ui.Text(manager, button);
		text.text = "Resume";
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

		button = new Ui.Button(manager, this);
		button.bounds.Width = manager.graphicsDevice.Viewport.Width;
		button.bounds.Height = fontTextHeight + 20;
		button.bounds.X = manager.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
		button.bounds.Y = manager.graphicsDevice.Viewport.Height / 2 + button.bounds.Height + 10;
		button.normalTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.normalTexture.SetData(new Color[] { buttonNormal });
		button.highlightTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.highlightTexture.SetData(new Color[] { buttonHighlight });
		button.pressedTexture = new Texture2D(manager.graphicsDevice, 1, 1);
		button.pressedTexture.SetData(new Color[] { buttonPress });
		button.onClickEvent += OnMainMenu;

		text = new Ui.Text(manager, button);
		text.text = "Main Menu";
		text.font = font;
		text.bounds.X = button.bounds.Width / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).X / 2f);
		text.bounds.Y = button.bounds.Height / 2 - GameMath.FloorToInt(text.font.MeasureString(text.text).Y / 2f);
		text.drawDepth = button.drawDepth + 1;

		base.Initialize();
	}

	void OnResume() {
		manager.SetCurrentRoot(null);
	}

	void OnMainMenu() {
		Game1.Instance.StartGame(null);
		manager.SetCurrentRoot(new MainMenu(manager));
	}
}

}