﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
/// <summary>
/// This is the main type for your game.
/// </summary>
public class Game1 : Game {
	private GraphicsDeviceManager graphics = null;
	private SpriteBatch spriteBatch = null;
	private Matrix globalTransform = Matrix.Identity;

	public Ui.Manager uiManager { get; private set; } = null;
	GameBase currentGame = null;

	public static Game1 Instance { get; private set; } = null;
	public string ContentRoot { get; private set; } = "";

	public Game1() {
		Instance = this;

		// Setup graphics
		graphics = new GraphicsDeviceManager(this);
		graphics.IsFullScreen = false;
		graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;
		graphics.SynchronizeWithVerticalRetrace = false;
		IsFixedTimeStep = false;

		// Setup Content
		if(System.IO.Directory.Exists("Content")) {
			ContentRoot = Content.RootDirectory = "Content";
		} else {
			ContentRoot = Content.RootDirectory = System.IO.Path.GetFullPath("../../../TimeWasters/Content/bin/DesktopGL");
			if(!System.IO.Directory.Exists(ContentRoot)) {
				throw new System.Exception("Failed to find Content Directory!");
			}
		}
	}

	/// <summary>
	/// Allows the game to perform any initialization it needs to before starting to run.
	/// This is where it can query for any required services and load any non-graphic
	/// related content.  Calling base.Initialize will enumerate through any components
	/// and initialize them as well.
	/// </summary>
	protected override void Initialize() {
		base.Initialize();
	}

	/// <summary>
	/// LoadContent will be called once per game and is the place to load
	/// all of your content.
	/// </summary>
	protected override void LoadContent() {
		// Create a new SpriteBatch, which can be used to draw textures.
		spriteBatch = new SpriteBatch(GraphicsDevice);

		Debug.Initialize(GraphicsDevice);

		uiManager = new Ui.Manager();
		if(!uiManager.Initialize(GraphicsDevice)) {
			Debug.LogError("Failed to initialize Ui Manager!");
			Exit();
		}
		uiManager.SetCurrentRoot(new TitleScreen(uiManager));

		uiManager.cursor = new Texture2D(GraphicsDevice, 1, 1);
		uiManager.cursor.SetData(new Color[] { Color.Red });
	}

	/// <summary>
	/// Allows the game to run logic such as updating the world,
	/// checking for collisions, gathering input, and playing audio.
	/// </summary>
	/// <param name="gameTime">Provides a snapshot of timing values.</param>
	protected override void Update(GameTime gameTime) {
		Time.Update(gameTime);

		// For Mobile devices, this logic will close the Game when the Back button is pressed
		// Exit() is obsolete on iOS
#if !__IOS__ &&  !__TVOS__
		//if(GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
		//	Exit();
#endif
		if(currentGame != null) {
			currentGame.Update();
		}

		uiManager.Update();
		base.Update(gameTime);
	}

	/// <summary>
	/// This is called when the game should draw itself.
	/// </summary>
	/// <param name="gameTime">Provides a snapshot of timing values.</param>
	protected override void Draw(GameTime gameTime) {
		graphics.GraphicsDevice.Clear(Color.CornflowerBlue);
		spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, globalTransform);

		if(currentGame != null) {
			currentGame.Draw();
		}

		spriteBatch.End();

		uiManager.Draw();
		Debug.Draw();
		base.Draw(gameTime);
	}

	public void StartGame(GameBase game) {
		currentGame = game;
		if(currentGame != null) {
			currentGame.Initialize(GraphicsDevice);
		}
	}
}
}

