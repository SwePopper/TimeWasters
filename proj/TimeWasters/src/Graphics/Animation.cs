using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class Animation {
	public Texture2D texture = null;
	
	Rectangle[] frames = new Rectangle[0];
	
	double animationTime = 0.0;
	int fps = 24;
	public int framesPerSecond { get { return fps; }
		set { 
			fps = value;
			animationTime = (double)frames.Length / (double)fps;
			frameTime = 0.0;
		}
	}
	
	public bool isPlaying = false;
	public bool isLooping = false;
	public double frameTime = 0.0;
	
	public void AddFrame(Rectangle frame) {
		System.Array.Resize(ref frames, frames.Length+1);
		frames[frames.Length-1] = frame;
		framesPerSecond = framesPerSecond; // Recalc values
	}
	
	public Rectangle GetCurrentFrame() {
		int frame = GameMath.FloorToInt(frameTime * fps);
		if(frame < 0 || frame > frames.Length)
			return Rectangle.Empty;
		return frames[frame];
	}
	
	public void Update() {
		if(isPlaying) {
			frameTime += Time.smoothDeltaTime;
			if(frameTime > animationTime) {
				if(isLooping) {
					frameTime -= animationTime;
				} else {
					isPlaying = false;
					frameTime = 0.0;
				}
			}
		}
	}
	
	public void Draw(SpriteBatch spriteBatch, Point position, bool flip) {
		Rectangle frame = GetCurrentFrame();
		if(frame == Rectangle.Empty)
			return;
		Rectangle dest = new Rectangle(position.X - frame.Size.X / 2, position.Y - frame.Size.Y, frame.Width, frame.Height);
		if(dest.Intersects(spriteBatch.GraphicsDevice.Viewport.Bounds)) {
			if(flip) {
				frame.X += frame.Width;
				frame.Width = -frame.Width;
			}
			spriteBatch.Draw(texture, dest, frame, Color.White);
		}
	}
}

}