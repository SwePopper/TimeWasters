using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
namespace Ui {

public class Image : Widget {
	public Texture2D texture = null;

	public Image(Manager manager, Widget parent = null) : base(manager, parent) {
		this.manager.AddDrawable(this);
	}

	public override void Draw(SpriteBatch spriteBatch) {
		if(texture != null) {
			Rectangle rect = new Rectangle(bounds.Location, bounds.Size);
			Widget parent = GetParent();
			while(parent != null) {
				rect.X += parent.bounds.X;
				rect.Y += parent.bounds.Y;
				parent = parent.GetParent();
			}
			spriteBatch.Draw(texture, rect, Color.White);
		}
		base.Draw(spriteBatch);
	}

	public override bool CanSelect() {
		return base.CanSelect() && false;
	}
}

}
}