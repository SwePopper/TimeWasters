using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
namespace Ui {

public class Text : Widget {
	public SpriteFont font = null;
	public string text = "";
	public Color textColor = Color.White;
	public Color textShadowColor = Color.Black;
	public bool drawShadow = true;

	public Text(Manager manager, Widget parent = null) : base(manager, parent) {
		this.manager.AddDrawable(this);
	}

	public override void Draw(SpriteBatch spriteBatch) {
		if(!string.IsNullOrEmpty(text) && font != null) {
			Vector2 position = bounds.Location.ToVector2();
			Widget parent = GetParent();
			while(parent != null) {
				position += parent.bounds.Location.ToVector2();
				parent = parent.GetParent();
			}

			if(drawShadow) {
				spriteBatch.DrawString(font, text, position + new Vector2(1.0f, 1.0f), textShadowColor);
			}
			spriteBatch.DrawString(font, text, position, textColor);
		}

		base.Draw(spriteBatch);
	}

	public override bool CanSelect() {
		return base.CanSelect() && false;
	}
}

}
}