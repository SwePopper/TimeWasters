using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
namespace Ui {

public class Widget {
	protected Manager manager = null;

	public Rectangle bounds = new Rectangle(0, 0, 1, 1);

	Widget parent = null;
	public List<Widget> children { get; private set; } = new List<Widget>();

	public bool hidden { get; set; } = false;
	public int drawDepth { get; set; }

	public Widget(Manager manager, Widget parent = null) {
		this.manager = manager;
		SetParent(parent);
	}

	public Widget GetParent() { return parent; }
	public void SetParent(Widget parent) {
		if(this.parent != null) {
			this.parent.children.Remove(this);
		}

		if(parent != null) {
			this.parent = parent;
			parent.children.Add(this);
		}
	}

	public void SetHightWithAspect(int width, Texture2D texture) {
		if(width <= 0)
			return;
		
		bounds.Width = width;
		bounds.Height = GameMath.FloorToInt(((float)texture.Height / (float)texture.Width) * width);
	}

	public void SetWidthWithAspect(int height, Texture2D texture) {
		bounds.Width = GameMath.FloorToInt(height / ((float)texture.Height / (float)texture.Width));
		bounds.Height = height;
	}

	public virtual void Draw(SpriteBatch spriteBatch) {}

	public virtual void OnAccept() {}
	public virtual bool CanSelect() { return true; }
	public virtual void OnSelect() {}
	public virtual void OnDeselect() {}
	public virtual void OnPressed() {}
}

}
}