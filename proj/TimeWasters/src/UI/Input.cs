using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TimeWasters {
namespace Ui {

public class Input {
	public delegate void OnPointerMovementEvent(Point newPosition, Point movement);
	public event OnPointerMovementEvent onPointerMovement = null;

	public delegate void OnPointerEvent(Point position);
	public event OnPointerEvent onPointerPressed = null;
	public event OnPointerEvent onPointerReleased = null;

	public delegate void OnEvent();
	public event OnEvent onAccept = null;
	public event OnEvent onBack = null;
	public event OnEvent onMovedUp = null;
	public event OnEvent onMovedLeft = null;
	public event OnEvent onMovedDown = null;
	public event OnEvent onMovedRight = null;

	public delegate void OnKeyboardKeyPressedEvent(Keys key);
	public event OnKeyboardKeyPressedEvent onKeyboardKeyPressed = null;

	public Keys[] acceptKeyboardKeys = new Keys[] { Keys.Enter, Keys.Space };
	public Keys[] backKeyboardKeys = new Keys[] { Keys.Escape, Keys.Back };
	public Keys[] upKeyboardKeys = new Keys[] { Keys.Up, Keys.W };
	public Keys[] leftKeyboardKeys = new Keys[] { Keys.Left, Keys.A };
	public Keys[] downKeyboardKeys = new Keys[] { Keys.Down, Keys.S };
	public Keys[] rightKeyboardKeys = new Keys[] { Keys.Right, Keys.D };

	MouseState lastMouseState;
	KeyboardState lastKeyboardState;

	public Input() {
		lastMouseState = Mouse.GetState();
		lastKeyboardState = Keyboard.GetState();
	}

	public void Update() {
		UpdateMouseState();
		UpdateKeyboard();
	}

#region Mouse Update

	void UpdateMouseState() {
		MouseState currentMouse = Mouse.GetState();
		if(!lastMouseState.Equals(currentMouse)) {
			Point movement = currentMouse.Position - lastMouseState.Position;
			if(!movement.IsZero()) {
				onPointerMovement?.Invoke(currentMouse.Position, movement);
			}

			// Check left mouse pressed and released state
			if(currentMouse.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released) {
				onPointerPressed?.Invoke(currentMouse.Position);
			} else if(currentMouse.LeftButton == ButtonState.Released && lastMouseState.LeftButton == ButtonState.Pressed) {
				onPointerReleased?.Invoke(currentMouse.Position);
			}

			lastMouseState = currentMouse;
		}
	}

#endregion

#region Keyboard Update

	void UpdateKeyboard() {
		KeyboardState currentKeyboard = Keyboard.GetState();
		if(!lastKeyboardState.Equals(currentKeyboard)) {
			if(IsKeysReleased(lastKeyboardState, currentKeyboard, acceptKeyboardKeys)) {
				onAccept?.Invoke();
			}

			if(IsKeysReleased(lastKeyboardState, currentKeyboard, backKeyboardKeys)) {
				onBack?.Invoke();
			}

			if(IsKeysReleased(lastKeyboardState, currentKeyboard, upKeyboardKeys)) {
				onMovedUp?.Invoke();
			}

			if(IsKeysReleased(lastKeyboardState, currentKeyboard, leftKeyboardKeys)) {
				onMovedLeft?.Invoke();
			}

			if(IsKeysReleased(lastKeyboardState, currentKeyboard, downKeyboardKeys)) {
				onMovedDown?.Invoke();
			}

			if(IsKeysReleased(lastKeyboardState, currentKeyboard, rightKeyboardKeys)) {
				onMovedRight?.Invoke();
			}

			if(onKeyboardKeyPressed != null) {
				Keys[] pressedKeys = currentKeyboard.GetPressedKeys();
				for(int i = 0; i < pressedKeys.Length; ++i) {
					onKeyboardKeyPressed.Invoke(pressedKeys[i]);
				}
			}

			lastKeyboardState = currentKeyboard;
		}
	}

	bool IsKeysPressed(KeyboardState last, KeyboardState cur, Keys[] keys) {
		for(int i = 0; i < keys.Length; ++i) {
			if(last.IsKeyUp(keys[i]) && cur.IsKeyDown(keys[i]))
				return true;
		}
		return false;
	}

	bool IsKeysReleased(KeyboardState last, KeyboardState cur, Keys[] keys) {
		for(int i = 0; i < keys.Length; ++i) {
			if(last.IsKeyDown(keys[i]) && cur.IsKeyUp(keys[i]))
				return true;
		}
		return false;
	}

#endregion
}

}
}