using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TimeWasters {
namespace Ui {

public class Manager {
	SpriteBatch spriteBatch = null;
	public GraphicsDevice graphicsDevice { get { return spriteBatch?.GraphicsDevice; } }

	public Texture2D cursor = null;

	Root currentRoot = null;
	List<Widget> selectables = new List<Widget>();
	List<Widget> drawables = new List<Widget>();

	Widget currentSelected = null;
	Widget pressedWidget = null;

	public Input input { get; private set; }
	public bool hasRoot { get { return currentRoot != null; } }

	public Manager() {
	}

	public bool Initialize(GraphicsDevice graphicsDevice) {
		spriteBatch = new SpriteBatch(graphicsDevice);
		input = new Input();
		input.onPointerMovement += OnPointerMovementEvent;
		input.onPointerPressed += OnLeftPressed;
		input.onPointerReleased += OnLeftReleased;
		input.onAccept += OnAccept;
		input.onBack += OnBack;
		input.onMovedUp += OnMoveUp;
		input.onMovedLeft += OnMoveLeft;
		input.onMovedDown += OnMoveDown;
		input.onMovedRight += OnMoveRight;
		return true;
	}

	public void Update() {
		// Remove null objects
		selectables.RemoveAll(x => x == null);
		input.Update();
	}

	public void Draw() {
		// Remove null objects
		drawables.RemoveAll(x => x == null);
		// Sort back to front so alpha should be ok
		drawables.Sort((x, y) => { return x.drawDepth - y.drawDepth; });

		spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
		for(int i = 0; i < drawables.Count; ++i) {
			// If not hidden and is inside viewport
			if(!drawables[i].hidden && drawables[i].bounds.Intersects(spriteBatch.GraphicsDevice.Viewport.Bounds)) {
				drawables[i].Draw(spriteBatch);
			}
		}

		if(cursor != null) {
			spriteBatch.Draw(cursor, Mouse.GetState().Position.ToVector2(), Color.White);
		}

		spriteBatch.End();
	}

	public void SetCurrentRoot(Root root) {
		selectables.Clear();
		drawables.Clear();
		currentSelected = null;
		pressedWidget = null;
		currentRoot = root;
		if(currentRoot != null) {
			currentRoot.Initialize();
		}
	}

	public void AddSelectable(Widget selectable) {
		if(selectable != null) {
			selectables.Add(selectable);
			if(currentSelected == null && selectable.CanSelect()) {
				currentSelected = selectable;
				currentSelected.OnSelect();
			}
		}
	}

	public void AddDrawable(Widget drawable) {
		if(drawable != null) {
			drawables.Add(drawable);
		}
	}

	public void Select(Widget widget) {
		if(pressedWidget != null || widget == null || !widget.CanSelect() || currentSelected == widget)
			return;
		
		if(currentSelected != null) {
			currentSelected.OnDeselect();
		}
		currentSelected = widget;
		currentSelected.OnSelect();
	}

#region Input Events

	void OnPointerMovementEvent(Point newPosition, Point movement) {
		if(pressedWidget != null)
			return;
		
		for(int i = 0; i < selectables.Count; ++i) {
			if(selectables[i].CanSelect() && selectables[i].bounds.Contains(newPosition)) {
				if(currentSelected == selectables[i])
					return;
				
				if(currentSelected != null) {
					currentSelected.OnDeselect();
				}
				currentSelected = selectables[i];
				currentSelected.OnSelect();
				break;
			}
		}
	}

	void OnLeftPressed(Point position) {
		if(currentSelected == null)
			return;
		
		if(currentSelected.bounds.Contains(position)) {
			currentSelected.OnPressed();
			pressedWidget = currentSelected;
		}
	}

	void OnLeftReleased(Point position) {
		if(pressedWidget != null && pressedWidget.bounds.Contains(position)) {
			pressedWidget.OnAccept();
			// Might disapear
			if(pressedWidget != null) {
				pressedWidget.OnSelect();
				currentSelected = pressedWidget;
			}
		} else if(pressedWidget != null) {
			pressedWidget.OnSelect();
		}
		pressedWidget = null;
	}

	public void OnAccept() {
		if(currentSelected != null) {
			currentSelected.OnAccept();
		}
	}

	public void OnBack() {
		if(currentRoot != null) {
			currentRoot.OnBack();
		}
	}

#region Selection Movement

	public void OnMoveUp() {
		if(pressedWidget != null)
			return;
		
		if(currentSelected == null) {
			SelectAny();
		} else {
			Widget nextSelection = GetNextSelection((cur, next) => 
				Math.Sign(cur.bounds.Y - next.bounds.Y) * (next.bounds.Location - cur.bounds.Location).LengthSquared()
			);
			if(nextSelection != null) {
				currentSelected.OnDeselect();
				currentSelected = nextSelection;
				currentSelected.OnSelect();
			}
		}
	}

	public void OnMoveLeft() {
		if(pressedWidget != null)
			return;
		
		if(currentSelected == null) {
			SelectAny();
		} else {
			Widget nextSelection = GetNextSelection((cur, next) => 
				Math.Sign(cur.bounds.X - next.bounds.X) * (next.bounds.Location - cur.bounds.Location).LengthSquared()
			);
			if(nextSelection != null) {
				currentSelected.OnDeselect();
				currentSelected = nextSelection;
				currentSelected.OnSelect();
			}
		}
	}

	public void OnMoveDown() {
		if(pressedWidget != null)
			return;
		
		if(currentSelected == null) {
			SelectAny();
		} else {
			Widget nextSelection = GetNextSelection((cur, next) => 
				Math.Sign(next.bounds.Y - cur.bounds.Y) * (next.bounds.Location - cur.bounds.Location).LengthSquared()
			);
			if(nextSelection != null) {
				currentSelected.OnDeselect();
				currentSelected = nextSelection;
				currentSelected.OnSelect();
			}
		}
	}

	public void OnMoveRight() {
		if(pressedWidget != null)
			return;
		
		if(currentSelected == null) {
			SelectAny();
		} else {
			Widget nextSelection = GetNextSelection((cur, next) => 
				Math.Sign(next.bounds.X - cur.bounds.X) * (next.bounds.Location - cur.bounds.Location).LengthSquared()
			);
			if(nextSelection != null) {
				currentSelected.OnDeselect();
				currentSelected = nextSelection;
				currentSelected.OnSelect();
			}
		}
	}

	Widget GetNextSelection(Comparison<Widget> comparison) {
		Widget nextSelection = null;
		int currentDistance = int.MaxValue;

		for(int i = 0; i < selectables.Count; ++i) {
			if(selectables[i].CanSelect() && selectables[i] != currentSelected) {
				int distance = comparison(currentSelected, selectables[i]);
				if(distance > 0 && distance < currentDistance) {
					currentDistance = distance;
					nextSelection = selectables[i];
				}
			}
		}

		return nextSelection;
	}
		
	void SelectAny() {
		if(currentSelected == null) {
			for(int i = 0; i < selectables.Count; ++i) {
				if(selectables[i].CanSelect()) {
					currentSelected = selectables[i];
					currentSelected.OnSelect();
					break;
				}
			}
		}
	}

#endregion

#endregion
}

}
}