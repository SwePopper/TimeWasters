using Microsoft.Xna.Framework.Content;

namespace TimeWasters {
namespace Ui {

public class Root : Widget {
	protected ContentManager contentManager = null;

	public Root(Manager manager) : base(manager, null) {
		contentManager = new ContentManager(Game1.Instance.Services, Game1.Instance.ContentRoot);
	}

	public virtual void Initialize() {}

	public override bool CanSelect() { return base.CanSelect() && false; }
	public virtual void OnBack() {}
}

}
}