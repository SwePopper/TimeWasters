using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
namespace Ui {

public class Button : Widget {
	public enum State {
		kNormal,
		kHighlight,
		kPressed,
		kDisabled
	}
	public delegate void OnClickEvent();

	public Texture2D normalTexture = null;
	public Texture2D highlightTexture = null;
	public Texture2D pressedTexture = null;
	public Texture2D disabledTexture = null;

	public event OnClickEvent onClickEvent = null;

	State currentState = State.kNormal;

	public bool disabled { 
		get { return currentState == State.kDisabled; } 
		set { currentState = value ? State.kDisabled : currentState == State.kDisabled ? State.kNormal : currentState; }
	}

	public Button(Manager manager, Widget parent = null) : base(manager, parent) {
		this.manager.AddDrawable(this);
		this.manager.AddSelectable(this);
	}

	public override void Draw(SpriteBatch spriteBatch) {
		Texture2D texture = normalTexture;
		if(currentState == State.kHighlight && highlightTexture != null) {
			texture = highlightTexture;
		} else if(currentState == State.kPressed && pressedTexture != null) {
			texture = pressedTexture;
		} else if(currentState == State.kDisabled && disabledTexture != null) {
			texture = disabledTexture;
		}

		if(texture != null) {
			Rectangle rect = new Rectangle(bounds.Location, bounds.Size);
			Widget parent = GetParent();
			while(parent != null) {
				rect.X += parent.bounds.X;
				rect.Y += parent.bounds.Y;
				parent = parent.GetParent();
			}
			spriteBatch.Draw(texture, rect, Color.White);
		}
		base.Draw(spriteBatch);
	}

	public override void OnAccept() {
		if(onClickEvent != null) {
			onClickEvent.Invoke();
		}
	}

	public override bool CanSelect() {
		return base.CanSelect() && currentState != State.kDisabled;
	}

	public override void OnSelect() {
		if(currentState != State.kDisabled) {
			currentState = State.kHighlight;
		}
		base.OnSelect();
	}

	public override void OnDeselect() {
		if(currentState != State.kDisabled) {
			currentState = State.kNormal;
		}
		base.OnDeselect();
	}

	public override void OnPressed() {
		currentState = State.kPressed;
		base.OnPressed();
	}
}

}
}