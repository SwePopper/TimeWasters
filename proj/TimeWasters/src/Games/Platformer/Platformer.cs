using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
public class Platformer : GameBase {
	Player player = null;
	
	public Platformer() {
	}

	public override bool Initialize(GraphicsDevice graphicsDevice) {
		string spriteSheetPath = Game1.Instance.ContentRoot + "/Player/p1_spritesheet.txt";
		using(System.IO.TextReader fileStream = System.IO.File.OpenText(spriteSheetPath)) {
			player = new Player(contentManager.Load<Texture2D>("Player/p1_spritesheet"), fileStream);
		}
		
		return base.Initialize(graphicsDevice);
	}

	protected override void OnStartingUpdate() {
		base.OnStartingUpdate();
		currentState = State.kPlaying;
	}

	protected override void OnPlayingUpdate() {
		player.Update();
		base.OnPlayingUpdate();
	}

	public override void Draw() {
		spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
		
		player.Draw(spriteBatch);
		
		spriteBatch.End();
		base.Draw();
	}
}
}

