using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {

public class Player {
	public enum AnimationState : int {
		kDuck,
		kFront,
		kHurt,
		kJump,
		kStand,
		kWalk,
		kCount
	}
	AnimationState currentAnimation = AnimationState.kStand;
	
	Animation[] animations = new Animation[0];
	
	Vector2 position = Vector2.Zero;
	Vector2 velocity = Vector2.Zero;
	
	float speed = 1000f;
	bool flip = false;
	bool onGround = false;
	
	public Player(Texture2D spriteSheet, TextReader spriteSheetInfo) {		
		animations = new Animation[(int)AnimationState.kCount];
		AddAnimation(AnimationState.kDuck, spriteSheet, spriteSheetInfo);
		AddAnimation(AnimationState.kFront, spriteSheet, spriteSheetInfo);
		AddAnimation(AnimationState.kHurt, spriteSheet, spriteSheetInfo);
		AddAnimation(AnimationState.kJump, spriteSheet, spriteSheetInfo);
		AddAnimation(AnimationState.kStand, spriteSheet, spriteSheetInfo);
		
		for(int i = 0; i < 11; ++i)
			AddAnimation(AnimationState.kWalk, spriteSheet, spriteSheetInfo);
		currentAnimation = AnimationState.kWalk;
		animations[(int)currentAnimation].isLooping = true;
		animations[(int)currentAnimation].isPlaying = true;
	}
	
	void AddAnimation(AnimationState state, Texture2D spriteSheet, TextReader spriteSheetInfo) {
		string line = spriteSheetInfo.ReadLine();
		string[] tokens = line.Split(' ');
		if(animations[(int)state] == null) {
			animations[(int)state] = new Animation();
			animations[(int)state].texture = spriteSheet;
		}
		animations[(int)state].AddFrame(new Rectangle(int.Parse(tokens[2]), int.Parse(tokens[3]), int.Parse(tokens[4]), int.Parse(tokens[5])));
	}
	
	public void Update() {
		bool isMoving = false;
		KeyboardState state = Keyboard.GetState(); 
		Vector2 movement = Vector2.Zero;
		if(onGround && state.IsKeyDown(Keys.Space)) {
			movement.Y -= 500000 * (float)Time.smoothDeltaTime;
			onGround = false;
		}
		if(state.IsKeyDown(Keys.Left) || state.IsKeyDown(Keys.A)) {
			movement.X -= speed * (float)Time.smoothDeltaTime;
			isMoving = true;
			flip = true;
		}
		if(state.IsKeyDown(Keys.Right) || state.IsKeyDown(Keys.D)) {
			movement.X += speed * (float)Time.smoothDeltaTime;
			isMoving = true;
			flip = false;
		}
		
		movement.Y += 500f * (float)Time.smoothDeltaTime;
		velocity += movement;

		position += velocity * (float)Time.smoothDeltaTime;
		
		if(!onGround) {
			ChangeAnimation(AnimationState.kJump);
		} else if(isMoving) {
			ChangeAnimation(AnimationState.kWalk);
		} else {
			ChangeAnimation(AnimationState.kStand);
		}
		
		animations[(int)currentAnimation].Update();
		
		Rectangle rect = animations[(int)currentAnimation].GetCurrentFrame();
		rect.Location = new Point(GameMath.FloorToInt(position.X - rect.Size.X / 2), GameMath.FloorToInt(position.Y - rect.Size.Y));
		
		if(rect.Bottom > Game1.Instance.GraphicsDevice.Viewport.Bounds.Bottom) {
			position.Y = Game1.Instance.GraphicsDevice.Viewport.Bounds.Height;
			velocity.Y = 0.0f;
			onGround = true;
		}
	}
	
	void ChangeAnimation(AnimationState state) {
		if(currentAnimation != state) {
			animations[(int)currentAnimation].frameTime = 0.0;
			currentAnimation = state;
		}
	}
	
	public void Draw(SpriteBatch spriteBatch) {
		animations[(int)currentAnimation].Draw(spriteBatch, position.ToPoint(), flip);
	} 
}

}