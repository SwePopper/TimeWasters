﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TimeWasters {
public class Corridor : GameBase {
	static readonly int kMinHeight = 15;
	static readonly int kMinWidth = 50;
	static readonly int groundCenterMovement = 10;

	Random random = new Random();

	Texture2D groundTexture = null;
	Texture2D playerTexture = null;
	Texture2D startTexture = null;

	Rectangle player;
	Rectangle startPosition;

	int groundSpeed = 1000;

	class GroundInfo {
		public double positionY = 0.0;
		public int center = 0;
		public int width = 0;
		public int height = 0;

		public Rectangle GetRect() {
			return new Rectangle(center - width / 2,
				(int)positionY,
				width,
				height);
		}
	}
	GroundInfo[] ground = null;

	public int points { get; private set; } = 0;

	public Corridor() {
	}

	public override bool Initialize(GraphicsDevice graphicsDevice) {
		currentState = State.kStarting;

		groundTexture = new Texture2D(graphicsDevice, 1, 1);
		groundTexture.SetData(new Color[] { Color.White });

		playerTexture = new Texture2D(graphicsDevice, 1, 1);
		playerTexture.SetData(new Color[] { Color.Red });

		startTexture = new Texture2D(graphicsDevice, 1, 1);
		startTexture.SetData(new Color[] { Color.Green });

		ground = new GroundInfo[(graphicsDevice.Viewport.Height / kMinHeight) + 20];
		ground[0] = new GroundInfo();
		ground[0].center = graphicsDevice.Viewport.Width / 2;
		ground[0].width = graphicsDevice.Viewport.Width / 2;
		ground[0].height = graphicsDevice.Viewport.Bounds.Height / 2;
		ground[0].positionY = graphicsDevice.Viewport.Bounds.Height - ground[0].height;
		for(int i = 1; i < ground.Length; ++i) {
			ground[i] = GetNextGround(ground[i - 1]);
		}

		startPosition = new Rectangle(graphicsDevice.Viewport.Width / 2, graphicsDevice.Viewport.Height - 100, 50, 50);

		return base.Initialize(graphicsDevice);
	}

	protected override void OnStartingUpdate() {
		base.OnStartingUpdate();

		Point position = Mouse.GetState().Position;
		player = new Rectangle(position.X - 5, position.Y - 5, 10, 10);

		if(player.Intersects(startPosition)) {
			currentState = State.kPlaying;
		}
	}

	protected override void OnPlayingUpdate() {
		base.OnPlayingUpdate();

		Point position = Mouse.GetState().Position;
		player = new Rectangle(position.X - 5, position.Y - 5, 10, 10);

		bool playerFailed = true;

		for(int i = 0; i < ground.Length; ++i) {
			if(playerFailed && ground[i].GetRect().Intersects(player))
				playerFailed = false;

			ground[i].positionY += groundSpeed * Time.deltaTime;
			if(ground[i].positionY > spriteBatch.GraphicsDevice.Viewport.Height) {
				if(i == 0) {
					ground[i] = GetNextGround(ground[ground.Length - 1]);
				} else {
					ground[i] = GetNextGround(ground[i - 1]);
				}
				++points;
			}
		}

		if(playerFailed) {
			currentState = State.kDone;
		}
	}

	public override void Draw() {
		spriteBatch.Begin(SpriteSortMode.Immediate);

		for(int i = 0; i < ground.Length; ++i) {
			Rectangle rect = ground[i].GetRect();
			if(rect.Intersects(spriteBatch.GraphicsDevice.Viewport.Bounds))
				spriteBatch.Draw(groundTexture, rect, Color.White);
		}

		if(currentState == State.kStarting) {
			spriteBatch.Draw(startTexture, startPosition, Color.Green);
		}

		spriteBatch.Draw(playerTexture, player, Color.Red);

		spriteBatch.End();

		base.Draw();
	}

	private GroundInfo GetNextGround(GroundInfo prev) {
		GroundInfo next = new GroundInfo();

		int maxLen = spriteBatch.GraphicsDevice.Viewport.Width / 2;
		int dirToCenter = maxLen - prev.center;
		double dirToCenterNorm = (double)dirToCenter / (double)maxLen;
		double leftWeight = 1.0;
		double rightWeight = 1.0;
		if(dirToCenterNorm < 0.0) {
			rightWeight = 1.0 - Math.Abs(dirToCenterNorm);
		} else if(dirToCenterNorm > 0.0) {
			leftWeight = 1.0 - dirToCenterNorm;
		}

		next.center = prev.center + random.Next(
			(int)(-groundCenterMovement * leftWeight),
			(int)(groundCenterMovement * rightWeight));
		
		next.width = GameMath.Min(prev.width - random.Next(-1, 5), kMinWidth);

		next.height = GameMath.Min(prev.height - 1, kMinHeight);
		next.positionY = prev.positionY - prev.height;
		return next;
	}
}
}

