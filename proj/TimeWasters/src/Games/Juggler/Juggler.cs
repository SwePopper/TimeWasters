﻿using Microsoft.Xna.Framework.Graphics;

namespace TimeWasters {
public class Juggler : GameBase {
	public Juggler() {
	}

	public override bool Initialize(GraphicsDevice graphicsDevice) {
		return base.Initialize(graphicsDevice);
	}

	protected override void OnStartingUpdate() {
		base.OnStartingUpdate();
		currentState = State.kPlaying;
	}

	protected override void OnPlayingUpdate() {
		base.OnPlayingUpdate();
		currentState = State.kDone;
	}

	public override void Draw() {
		base.Draw();
	}
}
}

