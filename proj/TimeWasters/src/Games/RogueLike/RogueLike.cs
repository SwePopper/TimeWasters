using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TimeWasters {
public class RogueLike : GameBase {
	Texture2D groundTexture = null;
	Texture2D wallTexture = null;
	Texture2D stairTexture = null;

	Dungeon dungeon = null;

	KeyboardState lastState;

	public RogueLike() {
	}

	public override bool Initialize(GraphicsDevice graphicsDevice) {
		groundTexture = new Texture2D(graphicsDevice, 1, 1);
		groundTexture.SetData(new Color[] { Color.DarkGray });

		wallTexture = new Texture2D(graphicsDevice, 1, 1);
		wallTexture.SetData(new Color[] { Color.Black });

		stairTexture = new Texture2D(graphicsDevice, 1, 1);
		stairTexture.SetData(new Color[] { Color.Green });

		dungeon = new Dungeon();

		lastState = Keyboard.GetState();

		return base.Initialize(graphicsDevice);
	}

	protected override void OnStartingUpdate() {
		base.OnStartingUpdate();
		currentState = State.kPlaying;
	}

	protected override void OnPlayingUpdate() {
		KeyboardState currentState = Keyboard.GetState();
		if(lastState.IsKeyDown(Keys.Enter) && currentState.IsKeyUp(Keys.Enter))
			dungeon.RandomWalkGeneration(1000);
		lastState = currentState;
		base.OnPlayingUpdate();
	}

	public override void Draw() {
		spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

		Texture2D[] textures = new Texture2D[] { wallTexture, groundTexture, stairTexture, stairTexture };

		spriteBatch.Draw(wallTexture, spriteBatch.GraphicsDevice.Viewport.Bounds, Color.White);

		Point size = new Point(5);
		for(int i = 0; i < dungeon.cells.Length; ++i) {
			if(dungeon.cells[i].type == Dungeon.CellType.kWall)
				continue;
			
			Rectangle rect = new Rectangle(new Point(i % dungeon.width, i / dungeon.width) * size, size);
			if(rect.Intersects(spriteBatch.GraphicsDevice.Viewport.Bounds))
				spriteBatch.Draw(textures[(int)dungeon.cells[i].type], rect, Color.White);
		}

		spriteBatch.End();

		base.Draw();
	}
}
}