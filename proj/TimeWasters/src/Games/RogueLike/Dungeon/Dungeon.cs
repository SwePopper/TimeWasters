using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace TimeWasters {

public class Dungeon {
	public enum CellType : int {
		kWall,
		kGround,
		kStart,
		kEnd
	}
	public class Cell {
		public CellType type = CellType.kWall;
	}
	public Cell[] cells = new Cell[0];
	public int width = 0;
	public int height = 0;

	Random random = null;

	public Dungeon() {}

#region BSP Alghorithm

	class BspNode {
		public Rectangle rect = Rectangle.Empty;

		public BspNode first = null;
		public BspNode second = null;
	}

	public void BSPGeneration(int worldSize = 200, int bspIterations = 4) {
		cells = new Cell[worldSize * worldSize];
		for(int i = 0; i < cells.Length; ++i)
			cells[i] = new Cell();
		
		width = height = worldSize;

		random = new Random(Guid.NewGuid().GetHashCode());
		GenerateBspNode(new Rectangle(1, 1, worldSize-2, worldSize-2), 0, bspIterations);
	}

	BspNode GenerateBspNode(Rectangle rect, int iteration, int maxIterations) {
		BspNode node = new BspNode();

		if(iteration < maxIterations) {
			Rectangle firstRect;
			Rectangle secondRect;
			if(rect.Width >= rect.Height) {
				// Split horizontally
				firstRect = new Rectangle(rect.Location, new Point(GameMath.FloorToInt(random.NextGaussian01() * rect.Width), rect.Height));
				secondRect = new Rectangle(rect.X + firstRect.Width, rect.Y, rect.Width - firstRect.Width, rect.Height);
			} else {
				// Split vertically
				firstRect = new Rectangle(rect.Location, new Point(rect.Width, GameMath.FloorToInt(random.NextGaussian01() * rect.Height)));
				secondRect = new Rectangle(rect.X, rect.Y + firstRect.Height, rect.Width, rect.Height - firstRect.Height);
			}

			// Generate Child Nodes
			node.first = GenerateBspNode(firstRect, iteration + 1, maxIterations);
			node.second = GenerateBspNode(secondRect, iteration + 1, maxIterations);

			// Connect Child Nodes with a corridor
		} else {
			// Randomize the position and size inside the rect
			Point size = new Point(GameMath.FloorToInt(random.NextGaussian01() * rect.Width), GameMath.FloorToInt(random.NextGaussian01() * rect.Height));
			rect.Location = rect.Location + new Point(random.Next(rect.Width - size.X), random.Next(rect.Height - size.Y));
			rect.Size = size;

			// Fill room
			SetCells(rect, CellType.kGround);
		}

		node.rect = rect;
		return node;
	}

	void SetCells(Rectangle rect, CellType cellType) {
		for(int j = rect.Y; j < rect.Y + rect.Height; ++j) {
			for(int i = rect.X; i < rect.X + rect.Width; ++i) {
				int cellIndex = j * width + i;
				cells[cellIndex].type = cellType;
			}
		}
	}

#endregion

#region Random Walk Algorithm
	
	public void RandomWalkGeneration(int walkableCount) {
		width = 0;
		height = 0;
		Point minPosition = new Point(0);
		Point currentPosition = new Point(0);
		List<Point> tempCells = new List<Point>();
		tempCells.Add(currentPosition);
		random = new Random(Guid.NewGuid().GetHashCode());

		// Walk a path randomly
		while(tempCells.Count < walkableCount) {
			switch(random.Next() % 4) {
			case 0:	currentPosition.X += 1; break;
			case 1:	currentPosition.X -= 1; break;
			case 2:	currentPosition.Y += 1; break;
			case 3:	currentPosition.Y -= 1; break;
			}

			if(tempCells.FindIndex(x => x == currentPosition) < 0) {
				minPosition.X = GameMath.Min(currentPosition.X, minPosition.X);
				minPosition.Y = GameMath.Min(currentPosition.Y, minPosition.Y);
				tempCells.Add(currentPosition);
			}
		}

		// Displace so that all positions (x and y) are greater than 0
		for(int i = 0; i < tempCells.Count; ++i) {
			Point temp = tempCells[i];
			temp.X += Math.Abs(minPosition.X) + 1;
			temp.Y += Math.Abs(minPosition.Y) + 1;
			tempCells[i] = temp;

			width = GameMath.Max(width, tempCells[i].X);
			height = GameMath.Max(height, tempCells[i].Y);
		}

		width += 2;
		height += 2;

		// Create cells
		cells = new Cell[width * height];
		for(int i = 0; i < cells.Length; ++i) {
			cells[i] = new Cell();
			if(tempCells.FindIndex(x => x.X == i % width && x.Y == i / width) >= 0) {
				cells[i].type = CellType.kGround;
			}
		}
	}
	
#endregion
}

}