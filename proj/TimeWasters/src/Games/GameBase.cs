﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TimeWasters {
public class GameBase {
	public enum State {
		kStarting,
		kPlaying,
		kPaused,
		kDone
	}
	public State currentState { get; protected set; } = State.kStarting;

	protected ContentManager contentManager = null;
	protected SpriteBatch spriteBatch = null;

	delegate void StateFunc();
	StateFunc[] stateFunc = null;

	public GameBase() {
		contentManager = new ContentManager(Game1.Instance.Services, Game1.Instance.ContentRoot);
	}

	public virtual bool Initialize(GraphicsDevice graphicsDevice) {
		spriteBatch = new SpriteBatch(graphicsDevice);
		stateFunc = new StateFunc[] { OnStartingUpdate, OnPlayingUpdate, OnPaused, OnDoneUpdate };
		return true;
	}

	public void Update() {
		stateFunc[(int)currentState]?.Invoke();
	}

	public virtual void Draw() {}

	protected virtual void OnStartingUpdate() {
	}

	protected virtual void OnPlayingUpdate() {
		if(GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) {
			currentState = State.kPaused;
			Game1.Instance.uiManager.SetCurrentRoot(new Pause(Game1.Instance.uiManager));
		}
	}

	void OnPaused() {
		if(!Game1.Instance.uiManager.hasRoot) {
			currentState = State.kPlaying;
		}
	}

	protected virtual void OnDoneUpdate() {}
}
}

