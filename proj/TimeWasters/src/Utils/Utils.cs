using System;

using Microsoft.Xna.Framework;

namespace TimeWasters {

public static class Utils {
	public static bool IsZero(this Point point) {
		return point.X == 0 && point.Y == 0;
	}

	public static void RemoveAt<T>(this T[] array, int index) {
		if(array == null || array.Length < index) {
			return;
		} else if(array.Length - 1 != index) {
			System.Array.Copy(array, index + 1, array, index, array.Length - index - 1);
		}
		System.Array.Resize(ref array, array.Length - 1);
	}

	public static void RemoveAll<T>(this T[] array, System.Predicate<T> match) {
		for(int i = 0; i < array.Length; ++i) {
			if(match(array[i])) {
				array.RemoveAt(i--);
			}
		}
	}

	public static bool IsNullOrEmpty<T>(this T[] array) {
		return array == null || array.Length <= 0;
	}
	
	public static double NextGaussian(this Random random, double mean = 0, double sigma = 1) {
		double u1 = random.NextDouble();
		double u2 = random.NextDouble();
		
		return mean + sigma * Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
	}
	
	public static double NextGaussian01(this Random random) {
		return GameMath.Clamp(random.NextGaussian(0.5, 0.25), 0.0, 1.0);
	}
}

}

