﻿using System;

using Microsoft.Xna.Framework;

namespace TimeWasters {

public static class GameMath {
	public static readonly double PI = Math.PI;
	public static readonly double PIhalf = PI/2.0;
	public static readonly double PI2 = PI*2.0;
	public static readonly float fPI = (float)Math.PI;
	public static readonly float fPIhalf = fPI/2f;
	public static readonly float fPI2 = fPI*2f;

	public static T Clamp<T>(T value, T min, T max) where T : IComparable {
		return value.CompareTo(min) < 0 ? min :
			value.CompareTo(max) > 0 ? max :
			value;
	}

	public static T Min<T>(T value, T min) where T : IComparable {
		return value.CompareTo(min) > 0 ? min :
			value;
	}

	public static T Max<T>(T value, T max) where T : IComparable {
		return value.CompareTo(max) < 0 ? max :
			value;
	}

	public static byte Lerp(byte from, byte to, double amount) {
		return (byte)(from + ((double)(to - from) * amount));
	}

	public static int FloorToInt(float value) {
		return (int)Math.Floor(value);
	}

	public static int FloorToInt(double value) {
		return (int)Math.Floor(value);
	}

	public static int Length(this Point rect) {
		return FloorToInt(Math.Sqrt((double)(rect.X * rect.X + rect.Y * rect.Y)));
	}

	public static int LengthSquared(this Point rect) {
		return FloorToInt(rect.X * rect.X + rect.Y * rect.Y);
	}
}

}

